CC=gcc
FLAGS=-Wall -Wpedantic -std=c99 -O0 -g

all: clean test
test:
	$(CC) rstring_test.c -o rstring_test $(FLAGS)

clean:
	rm -rf test

