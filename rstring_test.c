#include <stdio.h>
#include <assert.h>

#define RSTRING_IMPLEMENTATION
#include "./rstring.h"

int main(int argc, char **argv)
{
	// rstring_create
	{
		printf("[TEST] rstring_create()\n");
		rstring rs;
		rs = rstring_create();
		assert(rs.len == 0);

		rstring_free(&rs);
	}

	// rstring_create_from_cstr
	{
		printf("[TEST] rstring_create_from_cstr()\n");
		rstring rs;
		const char *source = "this is a c string :)";
		rs = rstring_create_from_cstr(source);

		size_t clength = strlen(source);
		assert(rs.len == clength && rs.cap == clength * 2);

		rstring_free(&rs);
	}

	// rstring_cmp equal
	{
		printf("[TEST] rstring_cmp() with equal strings\n");
		rstring rs1 = rstring_create_from_cstr("Hello, Juicer");
		rstring rs2 = rstring_create_from_cstr("Hello, Juicer");

		assert(rstring_cmp(&rs1, &rs2));

		rstring_free(&rs1);
		rstring_free(&rs2);
	}

	// rstring_cmp different
	{
		printf("[TEST] rstring_cmp() with different strings\n");
		rstring rs1 = rstring_create_from_cstr("Hello, Juicer");
		rstring rs2 = rstring_create_from_cstr("Hello, Golem");

		assert(rstring_cmp(&rs1, &rs2) == false);

		rstring_free(&rs1);
		rstring_free(&rs2);
	}

	// RSTRING macro
	{
		printf("[TEST] RSTRING macro\n");
		rstring rs = RSTRING("Hello, Juicer");

		assert(rs.len == sizeof("Hello, Juicer") - 1);

		rstring_free(&rs);
	}

	// RFREE macro
	{
		printf("[TEST] RFREE macro\n");
		rstring rs = RSTRING("Hello, Juicer");
		RFREE(rs);
		
		assert(rs.len == 0);
	}
	
	// rstring_push
	{
		printf("[TEST] rstring_push()\n");
		rstring rs1 = rstring_create_from_cstr("Hello, Juicer");
		rstring expected = rstring_create_from_cstr("Hello, Juicer!");
		rstring_push(&rs1, '!');

		assert(rstring_cmp(&rs1, &expected));

		rstring_free(&rs1);
		rstring_free(&expected);
	}

	// rstring_pop
	{
		printf("[TEST] rstring_pop()\n");
		rstring rs1 = rstring_create_from_cstr("Hello, Juicer!");
		rstring expected = rstring_create_from_cstr("Hello, Juicer");
		rstring_pop(&rs1);

		assert(rstring_cmp(&rs1, &expected));

		rstring_free(&rs1);
		rstring_free(&expected);
	}

	// rstring append
	{
		printf("[TEST] rstring_append()\n");
		rstring rs1 = RSTRING("Hello, ");
		rstring rs2 = RSTRING("Juicer");
		rstring expected = RSTRING("Hello, Juicer");
		rstring_append(&rs1, &rs2);

		assert(rstring_cmp(&rs1, &expected));
		assert(rs1.len == expected.len);

		RFREE(expected);
		RFREE(rs1);
		RFREE(rs2);
	}

	// rstring substr
	{
		printf("[TEST] rstring_substr()\n");
		rstring rs1 = RSTRING("Hello, Juicer");
		rstring sub = rstring_substr(&rs1, 7, 8);
		rstring expected = RSTRING("Juicer");

		assert(rstring_cmp(&sub, &expected));

		RFREE(expected);
		RFREE(rs1);
		RFREE(sub);
	}

	// rstring as cstr
	{
		printf("[TEST] rstring_as_cstr()\n");
		rstring rs = RSTRING("Hello, Juicer");
		char *value = rstring_as_cstr(&rs);
		const char *expected = "Hello, Juicer";

		assert(strcmp(value, expected) == 0);
		
		free(value);
		RFREE(rs);
	}
	
}
