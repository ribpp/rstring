#ifndef RSTRING_H_
#define RSTRING_

#include <stdbool.h>

#define DEFAULT_RSTRING_CAP 10

#define RStr_Fmt "%.*s"
#define RStr_Arg(rstr) (int)(rstr.len), rstr.buf

typedef struct RString {
	char *buf;
	size_t len;
	size_t cap;
} rstring;

rstring rstring_create();
rstring rstring_create_from_cstr(const char *cstr);
void rstring_free(rstring *rs);

void rstring_push(rstring *rs, char c);
void rstring_pop(rstring *rs);
void rstring_append(rstring *to, rstring *from);

rstring rstring_substr(rstring *rs, size_t start, size_t length);
bool rstring_cmp(rstring *rs1, rstring *rs2);

char *rstring_as_cstr(rstring *rs);
void rstring_clear(rstring *rs);

#endif // RSTRING_H_

#ifdef RSTRING_IMPLEMENTATION

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define RSTRING(cstr) rstring_create_from_cstr(cstr)
#define RFREE(rs) rstring_free(&rs)
#define RFREE_PTR(rs) rstring_free(rs)

rstring rstring_create()
{
	assert(DEFAULT_RSTRING_CAP > 0 && "DEFAULT_RSTRING_CAP is less or equal to 0.");
	rstring rs = { 0 };
	rs.buf = malloc(sizeof(char) * DEFAULT_RSTRING_CAP);
	if (rs.buf == NULL) {
		fprintf(stderr, "ERROR: could not allocate memory for rstring.\n");
		exit(1);
	}

	rs.cap = DEFAULT_RSTRING_CAP;
	return rs;
}

rstring rstring_create_from_cstr(const char *cstr)
{
	rstring rs;
	size_t length = strlen(cstr);
	rs.len = length;
	rs.cap = length * 2;
	rs.buf = malloc(sizeof(char) * rs.cap);
	memcpy(rs.buf, cstr, length);

	return rs;
}

void rstring_free(rstring *rs)
{
	free(rs->buf);
	rs->cap = rs->len = 0;
}

static void rstring_realloc(rstring *rs, size_t minimum)
{
	if (rs->cap * 2 >= minimum) {
		rs->cap *= 2;
	} else {
		rs->cap += minimum;
	}
	
	rs->buf = realloc(rs->buf, (sizeof(char) * rs->cap));
	if (rs->buf == NULL) {
	   fprintf(stderr, "ERROR: could not reallocate rstring.\n");
	   exit(1);
	}
}

void rstring_push(rstring *rs, char c)
{
	if (rs->len >= rs->cap) {
		rstring_realloc(rs, 0);
	}

	rs->buf[rs->len] = c;
	rs->len += 1;
}

void rstring_pop(rstring *rs)
{
	if (rs->len > 0) {
		rs->len -= 1;
	}
}

void rstring_append(rstring *to, rstring *from)
{
	if (to->len + from->len >= to->cap) {
		rstring_realloc(to, from->len);
	}

	memcpy(to->buf + (int)(to->len), from->buf, from->len);
	to->len += from->len;
}

rstring rstring_substr(rstring *rs, size_t start, size_t length)
{
	rstring result = rstring_create();

	if (start + length >= rs->len - start) {
		length = rs->len - start;
	}

	if (length >= result.cap) {
		rstring_realloc(&result, start + length);
	}
	
	memcpy(result.buf, rs->buf + (int)start, length);
	result.len = length;
	return result;
}

bool rstring_cmp(rstring *rs1, rstring *rs2)
{
	if (rs1->len != rs2->len) {
		return false;
	}

	return memcmp(rs1->buf, rs2->buf, rs1->len) == 0;
}

char *rstring_as_cstr(rstring *rs)
{
	char *cstr = malloc((sizeof(char) * rs->len) + 1);
	if (cstr == NULL) {
		fprintf(stderr, "ERROR: could not allocate string with malloc.\n");
		exit(1);
	}

	cstr[rs->len + 1] = '\0';

	memcpy(cstr, rs->buf, rs->len);
	return cstr;
}

void rstring_clear(rstring *rs)
{
	rs->len = 0;
}

#endif // RSTRING_IMPLEMENTATION
